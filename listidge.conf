# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: CC-BY-SA-4.0
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This work is licensed under the Creative Commons Attribution 4.0 International
# License. You should have received a copy of the license along with this work.
# If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
# Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

server {
  listen 80 default_server;

  # Security - disable disables emitting nginx version on error pages and in
  # the “Server” response header field. Note it still emits nginx:
  # [nginx "server_tokens off" does not remove the server header - Stack Overflow](https://stackoverflow.com/questions/20247184/nginx-server-tokens-off-does-not-remove-the-server-header)
  server_tokens off; 

  # https://kubernetes.github.io/ingress-nginx/deploy/hardening-guide/
  keepalive_time 5;
  proxy_hide_header X-Powered-By;
  client_header_timeout 5;
  client_body_timeout 5;
  large_client_header_buffers 4 1k;

  # Set headers for strict security policy
  add_header Permissions-Policy "accelerometer=(),autoplay=(),camera=(),display-capture=(),document-domain=(),encrypted-media=(),fullscreen=(),geolocation=(),gyroscope=(),magnetometer=(),microphone=(),midi=(),payment=(),picture-in-picture=(),publickey-credentials-get=(),screen-wake-lock=(),sync-xhr=(self),usb=(),web-share=(),xr-spatial-tracking=()";

  location /uploads  {
      alias /tmp/listidge;
      # Security - Django takes care of this, but not nginx
      add_header X-Content-Type-Options "nosniff";

      deny all;
      # User content, restrict files we will serve
      location ~* "\.(jpg|jpeg|png|webp|gif)$" {
        allow all;
      }
  }

  location /static {
      alias /app/staticfiles;
      # Security - Django takes care of this, but not nginx
      add_header X-Content-Type-Options "nosniff";
  }

  location / {
      include uwsgi_params;
      uwsgi_pass unix:///tmp/uwsgi.sock;
  }
}

