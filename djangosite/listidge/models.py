# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django.db import models
from django.contrib.auth.models import User


class ListType(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class List(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    uuid = models.UUIDField(null=True)
    listype = models.ForeignKey(ListType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ListGiftItem(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    link = models.CharField(max_length=500)
    amount = models.IntegerField(default=1)
    image = models.ImageField(upload_to='listgift/%Y/%m/%d/', null=True)

    def __str__(self):
        return self.name


class ListToDoItem(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    assigned = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    completed = models.BooleanField()

    def __str__(self):
        return self.name


class ListCollectionItem(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    link = models.CharField(max_length=500)
    image = models.ImageField(upload_to='listcollection/%Y/%m/%d/', null=True)

    def __str__(self):
        return self.name
