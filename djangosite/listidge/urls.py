# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = 'listidge'
urlpatterns = [
    path('', views.redirect, name='redirect'),
    path('_admin/lists/', views.lists, name='lists'),
    path('_admin/lists/<int:list_id>/', views.list, name='list'),
    path('_admin/lists/<int:list_id>/delete', views.list_delete,
         name='list_delete'),
    path('_admin/lists/<int:list_id>/<int:item_id>/delete', views.item_delete,
         name='item_delete'),
    path('_admin/lists/<int:list_id>/share', views.list_share_from_list,
         name='list_share_from_list'),
    path('_admin/lists/share/<int:list_id>', views.list_share_from_lists,
         name='list_share_from_lists'),
    path('login/', views.login_user, name='login_user'),
    path('logout/', views.logout_user, name='logout_user'),
    path('<uuid:list_uuid>', views.list_public, name='list_public'),
    path('<uuid:list_uuid>/reserve/<int:item_id>',
         views.item_reserve, name='item_reserve'),
    path('<uuid:list_uuid>/todoupdate/<int:item_id>',
         views.todoitem_update, name='todoitem_update'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# Note that the above static help function works only in DEBUG mode
# https://docs.djangoproject.com/en/4.1/howto/static-files/#serving-files-uploaded-by-a-user-during-development

if settings.REGISTRATION:
    urlpatterns += path('register/', views.register_user,
                        name='register_user'),
