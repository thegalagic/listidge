# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django import forms


class ListWishlistForm(forms.Form):
    name = forms.CharField(max_length=200, required=False)
    description = forms.CharField(max_length=200, required=False)
    link = forms.CharField(max_length=500, required=False)
    amount = forms.IntegerField(min_value=1, max_value=100, required=True)
    image = forms.ImageField(required=False)


class ListCollectionItemForm(forms.Form):
    title = forms.CharField(max_length=200, required=False)
    description = forms.CharField(max_length=200, required=False)
    link = forms.CharField(max_length=500, required=False)
    image = forms.ImageField(required=False)
