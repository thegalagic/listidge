# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import uuid
from .models import List, ListGiftItem, ListToDoItem, ListCollectionItem
from .forms import ListCollectionItemForm, ListWishlistForm


class WishlistManager:
    lst: List

    def __init__(self, lst):
        self.lst = lst

    def create_item(self, post, files):
        form = ListWishlistForm(post, files)

        if not form.is_valid():
            return (False, form.errors)

        user_image = None
        if form.cleaned_data['image'] is not None:
            user_image = form.cleaned_data['image']
            # Check content type, only accept common types for now
            # https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types
            if user_image.content_type not in ["image/jpeg",
                                               "image/gif",
                                               "image/png",
                                               "image/webp"]:
                return (False, {"image": ("Sorry we only accept images in the \
                                           formats jpeg, gif, png and webp.")})
            # Set to random name before we save, preserve extension
            user_image.name = (str(uuid.uuid4()) +
                               os.path.splitext(user_image.name)[1])
        lgi = ListGiftItem(name=post['name'],
                           description=post['description'],
                           link=post['link'],
                           amount=post['amount'],
                           image=user_image,
                           list_id=self.lst.id)
        lgi.save()
        return (True, None)

    def get_items(self):
        return ('listidge/list.html',
                ListGiftItem.objects.filter(list_id=self.lst.id))

    def get_public_items(self):
        return ('listidge/list_public.html',
                {'list': self.lst,
                 'items': ListGiftItem.objects.filter(list_id=self.lst.id,
                                                      amount__gt=0)})

    def delete_item(self, item_id):
        ltdi = ListGiftItem.objects.filter(pk=item_id)
        ltdi.delete()


class ToDoListManager:
    lst: List

    def __init__(self, lst):
        self.lst = lst

    def create_item(self, post, files):
        ltdi = ListToDoItem(name=post['name'],
                            description=post['description'],
                            assigned=post['assigned'],
                            state=post['state'],
                            completed='completed' in post,
                            list_id=self.lst.id)
        ltdi.save()
        return (True, None)

    def get_items(self):
        return ('listidge/todolist.html',
                ListToDoItem.objects.filter(list_id=self.lst.id))

    def get_public_items(self):
        todoitems = ListToDoItem.objects.filter(list_id=self.lst.id,
                                                state="To Do")
        doingitems = ListToDoItem.objects.filter(list_id=self.lst.id,
                                                 state="Doing")
        doneitems = ListToDoItem.objects.filter(list_id=self.lst.id,
                                                state="Done")
        return ('listidge/list_public_todo.html',
                {'list': self.lst,
                 "todoitems": todoitems,
                 "doingitems": doingitems,
                 "doneitems": doneitems})

    def delete_item(self, item_id):
        ltdi = ListToDoItem.objects.filter(pk=item_id)
        ltdi.delete()


class CollectionManager:
    lst: List

    def __init__(self, lst):
        self.lst = lst

    def create_item(self, post, files):
        form = ListCollectionItemForm(post, files)

        if not form.is_valid():
            return (False, form.errors)

        user_image = None
        if form.cleaned_data['image'] is not None:
            user_image = form.cleaned_data['image']
            # Check content type, only accept common types for now
            # https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types
            if user_image.content_type not in ["image/jpeg",
                                               "image/gif",
                                               "image/png",
                                               "image/webp"]:
                return (False, {"image": ("Sorry we only accept images in the \
                                           formats jpeg, gif, png and webp.")})
            # Set to random name before we save, preserve extension
            user_image.name = (str(uuid.uuid4()) +
                               os.path.splitext(user_image.name)[1])

        li = ListCollectionItem(title=form.cleaned_data['title'],
                                description=form.cleaned_data['description'],
                                link=form.cleaned_data['link'],
                                image=user_image,
                                list_id=self.lst.id)
        li.save()
        return (True, None)

    def get_items(self):
        return ('listidge/collection.html',
                ListCollectionItem.objects.filter(list_id=self.lst.id))

    def get_public_items(self):
        items = ListCollectionItem.objects.filter(list_id=self.lst.id)
        return ('listidge/list_public_collection.html',
                {'list': self.lst,
                 "items": items})

    def delete_item(self, item_id):
        li = ListCollectionItem.objects.filter(pk=item_id)
        li.delete()


def get_list_mgr(lst: List):
    match lst.listype.name:
        case "Wishlist":
            return WishlistManager(lst)
        case "To-do list":
            return ToDoListManager(lst)
        case "Collection":
            return CollectionManager(lst)
        case _:
            raise Exception(f"Unknown list type: {lst.listype.name}")
