<!--
SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

Self-hosted lists of any kind: wishlists, task lists, collections and more.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# listidge

![Listidge Logo](djangosite/listidge/static/listidge/logo.png)

[![Docker Pulls](https://img.shields.io/docker/pulls/iantwenty/listidge?style=for-the-badge)](https://hub.docker.com/r/iantwenty/listidge)
[![Mastodon Follow](https://img.shields.io/mastodon/follow/107938582007286992?domain=https%3A%2F%2Ffosstodon.org%2F&label=ON%20MASTODON&style=for-the-badge)](https://fosstodon.org/@fosstian)
[![Reddit User Karma](https://img.shields.io/reddit/user-karma/combined/IanTwenty?label=ON%20REDDIT&style=for-the-badge)](https://www.reddit.com/user/IanTwenty)
[![HackerNews User Karma](https://img.shields.io/hackernews/user-karma/ian_hn?label=ON%20HACKERNEWS&style=for-the-badge)](https://news.ycombinator.com/submitted?id=ian_hn)

Listidge lets you self-host and share lists of any thing you can
imagine. Wishlists, task lists, collections and more. Open source forever under
the AGPL license. Built with Python, Django and Bootstrap.

**Note**: this project is currently a prototype and should be used just for testing.

![Listidge showing three types of lists](docs/Screen Shot 2023-03-28 at 12.56.51.png)

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Install](#install)
  * [Via docker](#via-docker)
  * [Via source](#via-source)
  * [Configuration](#configuration)
  * [Data](#data)
* [Usage](#usage)
* [Current Limitations](#current-limitations)
* [Future](#future)
* [Contributing](#contributing)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

I've used wishlists for many years and wanted an alternative to
those that are proprietary or lock you into their platform. It also seemed like a
problem that could be generalised to sharing lists of any kind: to-dos, tasks,
collections etc.

In March 2023 I posted a
[question](https://www.reddit.com/r/selfhosted/comments/11mytph/should_i_build_a_selfhosted/)
on Reddit's r/selfhosted to see what people thought. I got enough interest to
think it was worth building a prototype, so here we are.

## Install

You have a choice how to install:

### Via docker

Listidge is on [Docker Hub](https://hub.docker.com/r/iantwenty/listidge) and
needs just a single container. We support x86 right now but ARM is coming soon:

```bash
docker run -p=8000:80 iantwenty/listidge:latest
```

Find the site at [http://127.0.0.1:8000/](http://127.0.0.1:8000/).

To build your own image you can use the provided `Dockerfile` in the repo:

```bash
podman build .
```

### Via source

Pre-requisites:

* python
* git

Steps:

```bash
# Clone the repo - choose ssh or http
git clone git@gitlab.com:thegalagic/listidge.git
git clone https://gitlab.com/thegalagic/listidge.git
#
cd listidge
# Create a virtual env and switch into it
python3 -m venv venv
. venv/bin/activate
# Install requirements
pip install -r requirements.txt
# Migrate the database
python3 djangosite/manage.py migrate
# Run the dev server in debug mode
DEBUG=True python3 manage.py runserver 8000
```

Find the site at [http://127.0.0.1:8000/](http://127.0.0.1:8000/).

### Configuration

There are a few settings you can control via environment variables:

* `DEBUG`: `True` or `False`. Extra information will be available if errors are
  experienced, don't use in production. Default false. For more info see
  [DEBUG](https://docs.djangoproject.com/en/4.1/ref/settings/#debug).
* `DEMO_USER`: `True` or `False`. Whether the user account demo/demo
  (user/password) is enabled. Default false.
* `REGISTRATION`: `True` or `False`. Whether user registration is allowed.
  Default true.
* `ALLOWED_HOSTS`: comma-separated list of hostnames to expect in HTTP requests.
  Set to your domain(s), e.g. `mydomain.com,myotherdomain.com` or can use a
  wildcard(not recommended). For more info see [ALLOWED_HOSTS](https://docs.djangoproject.com/en/4.1/ref/settings/#std-setting-ALLOWED_HOSTS).
* `DJANGO_SECRET_KEY`: used as a salt to secure various encrypted strings.
  Default is randomised at start-up, which is not suitable for long-term use.
  You'll need to decide your own policy for production use.
  See [SECRET_KEY](https://docs.djangoproject.com/en/4.1/ref/settings/#std-setting-SECRET_KEY).

### Data

Listidge stores two kinds of data:

* All config is stored in a sqlite database which will be called `db.sqlite3`.
  It's located at the root of the site.
* User uploaded content is stored in `/tmp/listidge`. This is intentionally
  temporary at the moment, due to our prototype status.

## Usage

If you prefer to learn with subtitled videos I'm posting short run-throughs on
Mastodon. Here's the first:

* [Create and share a wishlist (fosstodon.org)](https://fosstodon.org/@fosstian/110152148765385359)

Otherwise read on.

First login or register (no email required):

![Login screen](docs/Screen Shot 2023-03-28 at 12.59.09.png)

On the main lists page you can see all your lists and **create** new ones. A new
list is private by default, only visible to you.

![Listidge showing three types of lists](docs/Screen Shot 2023-03-28 at 12.56.51.png)

The prototype supports three **types** of list right now:

* Wishlists. A list of items each with a description, image, link and amount
  wanted. The public link lets anyone reserve items from the list.
* To-do lists. A list of tasks each with a description, assigned person, state
  and completed checkbox. The public link lets anyone change the state of a task
  and mark it completed.
* Collections. A list of items each with a description, image and link. The
  public link lets anyone view your collection.

For each of your lists you can choose to **publish** a public sharing link. This
link is unguessable, so only those you share it with can find it.

Click on a list's name and you're taken to its information page where you
can add/remove items from it:

![Edit a list](docs/Screenshot 2023-03-28 at 13-04-21 My birthday.png)

From this page you can also publish or delete your list.

Each type of list has a different **public** view. For **wishlists** the public
view allows anyone to reserve items:

![Public view of a wishlist](docs/Screen Shot 2023-03-28 at 13.29.45.png)

For **to-do lists** the public view let's you update the state of tasks and mark
them completed:

![Public view of a to-do list](docs/Screenshot 2023-03-28 at 13-36-53 Shopping.png)

For **collections** the public link only allows viewing:

![Pubic view of a collection](docs/Screenshot 2023-03-28 at 13-44-42 Favourite films.png)

## Current Limitations

* You can't edit list items, only add/remove.
* It's not suitable for production deployment. More safety and security is
  needed in several areas but particularly the handling of user-uploaded content.
* Images that are uploaded gain a secret but public URL even before the list is
  made public. They should be kept private until the list is made public.

## Future

This is just a prototype. In future we'd like to:

* Give the user complete control of the fields in their lists. Edit the field
  types, allowable values, conditions for state changes etc.
* Have different views for different lists. For example maybe you don't want to
  see the to-do list as a kanban-style board (as it is now) but in some other way.
* Different kinds of sharing - links for people to edit, links for people to
  view, links only for people who are logged in etc.
* Add more list types. List types could be like plugins and new ones could be
  developed by the community/available from some kind of directory.

We're looking for your ideas as well. How could listidge help you?

## Contributing

It's great that you're interested in contributing. Please ask questions by
raising an issue and PRs will be considered. For full details see
[CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

* All source code is licensed under AGPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
* Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

listidge is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
